import Axios from "axios";
import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);
const server = "http://localhost:3000/api/v1";

const actions = {
  async addUser({ commit }, user) {
    const newUser = await Axios.post(`${server}/users`, user)
      .then((resp) => resp.data)
      .catch((err) => console.log(err));
    commit("ADD_USER", newUser);
  },
  async getUsers({ commit }) {
    const users = await Axios.get(`${server}/users`)
      .then((resp) => resp.data)
      .catch((err) => console.log(err));
    commit("REPLACE_USERS", users);
  },
  async updateUser({ commit }, user) {
    const updatedUser = await Axios.put(`${server}/users/${user._id}`, user)
      .then((resp) => resp.data)
      .catch((err) => console.log(err));
    commit("UPDATE_USER", updatedUser);
  },
  async removeUser({ commit }, user) {
    const deletedUser = await Axios.delete(`${server}/users/${user._id}`)
      .then((resp) => resp.data)
      .catch((err) => console.log(err));
    if (deletedUser) {
      commit("REMOVE_USER", user._id);
    }
  },
};

const mutations = {
  ADD_USER(state, user) {
    state.users = [user, ...state.users];
  },
  REPLACE_USERS(state, users) {
    state.users = users;
  },
  REMOVE_USER(state, userId) {
    state.users = state.users.filter((u) => u._id !== userId);
  },
  UPDATE_USER(state, user) {
    const index = state.users.findIndex((u) => u._id === user._id);
    if (index !== -1) {
      state.users.splice(index, 1, user);
    }
  },
};

export default new Vuex.Store({
  state: {
    users: [],
  },
  mutations,
  actions,
  modules: {},
});
