import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from "../views/Home";
import FormUser from "../views/FormUser";
import ListUser from "../views/ListUser";


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/users',
    name: 'list-user',
    component: ListUser
  },
  {
    path: '/users/new',
    name: 'add-user',
    component: FormUser
  },
  {
    path: '/users/:id/edit',
    name: 'edit-user',
    component: FormUser,
    params:true
  },
  
]

const router = new VueRouter({
  routes,
  mode:"history"
})

export default router
