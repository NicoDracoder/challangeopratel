<?php

require_once __DIR__ . '/../config/Config.php';

class Connection
{
    private ?PDO $connection;

    public function __construct()
    {
        $this->connection = $this->connect(
            Config::DB_HOST,
            Config::DB_DATABASE,
            Config::DB_USER,
            Config::DB_PASSWORD
        );
    }

    public function connect(string $host, string $database, string $user, string $password): ?PDO
    {
        try {
            return new PDO("mysql:host=$host;dbname=$database", $user, $password);
        } catch (PDOException $e) {
            print "¡Error!: " . $e->getMessage() . "<br/>";

            return null;
        }
    }

    /**
     * @return PDO
     */
    public function getConnection(): PDO
    {
        if (!$this->connection) {
            $this->connection = $this->connect(
                Config::DB_HOST,
                Config::DB_DATABASE,
                Config::DB_USER,
                Config::DB_PASSWORD
            );
        }

        return $this->connection;
    }

    /**
     *
     */
    public function closeConnection(): void
    {
        $this->connection = null;
    }
}