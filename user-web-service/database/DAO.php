<?php

require_once 'Connection.php';

class DAO
{
    /**
     * @param string $password
     *
     * @return string
     */
    private function encryptPassword(string $password): string
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }

    /**
     * @param string $username
     * @param string $email
     * @param string $password
     *
     * @return bool
     */
    public function addUserDAO(
        string $username,
        string $email,
        string $password
    ): bool
    {
        try {
            $isActive = true;
            $conn = new Connection();
            $encryptedPassword = $this->encryptPassword($password);
            $query = "INSERT INTO users(username, email, password,isActive) VALUES (?,?,?,?);";
            $stmt = $conn->getConnection()->prepare($query);
            $stmt->execute([$username, $email, $encryptedPassword, $isActive]);
            $conn->getConnection();

            return true;
        } catch (PDOException $e) {
            error_log($e->getMessage());
        }

        return false;
    }


    /**
     * @param string $username
     *
     * @return array|null
     */
    public function getUserDAO(string $username): ?array
    {
        try {
            $conn = new Connection();
            $query = "SELECT * FROM users WHERE username=?;";
            $stmt = $conn->getConnection()->prepare($query);
            $stmt->execute([$username]);
            $user = $stmt->fetch();
            $conn->closeConnection();

            if ($user) {
                return $user;
            }
        } catch (PDOException $e) {
            error_log($e->getMessage());
        }

        return null;
    }

    /**
     * @param string $username
     * @param bool $isActive
     *
     * @return bool
     */
    public function changeActiveUserDAO(
        string $username,
        bool $isActive
    ): bool
    {
        try {
            $conn = new Connection();
            $query = "UPDATE users SET isActive=? WHERE username=?;";
            $stmt = $conn->getConnection()->prepare($query);
            $stmt->execute([$isActive, $username]);
            $conn->getConnection();

            return true;
        } catch (PDOException $e) {
            error_log($e->getMessage());
        }

        return false;
    }

}