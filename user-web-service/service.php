<?php

require_once "./libs/nusoap.php";
require_once "./utils/Utils.php";
require_once "./utils/Logger.php";
require_once "./services/UserService.php";

// create server
$server = new soap_server();
// database server
$namespace = "urn:myUserService";
$server->configureWSDL("Opratel user WS", $namespace);
$server->schemaTargetNamespace = $namespace;

//Register functions
registerService(
    $server,
    $namespace,
    "addUser",
    [
        "username" => "xsd:string",
        "email" => "xsd:string",
        "password" => "xsd:string",
    ],
    ["status_code " => "xsd:int"]
);
registerService(
    $server,
    $namespace,
    "activateUser",
    ["username" => "xsd:string"],
    ["status_code " => "xsd:int"]
);
registerService(
    $server,
    $namespace,
    "deactivateUser",
    ["username" => "xsd:string"],
    ["status_code " => "xsd:int"]
);
registerService(
    $server,
    $namespace,
    "getUser",
    ["username" => "xsd:string"],
    [
        "username" => "xsd:string",
        "email" => "xsd:string",
        "password" => "xsd:string",
    ]
);


$HTTP_RAW_POST_DATA = $HTTP_RAW_POST_DATA ?? '';
$server->service(file_get_contents("php://input"));
