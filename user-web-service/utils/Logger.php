<?php

class Logger
{
    private const FILENAME = "logs.php";
    private const LOG_FLAG_INFO = "INFO";
    private const LOG_FLAG_ERROR = "ERROR";

    /**
     * @param string $content
     */
    private static function addLog(string $content): void
    {
        file_put_contents(self::FILENAME, $content . PHP_EOL, FILE_APPEND);
    }


    /**
     * @return string
     */
    private static function getCurrentTime(): string
    {
        $now = new DateTime();
        $formatNow = $now->format('Y-m-d H:i:s');
        return "[$formatNow] ";
    }

    /**
     * @param array $params
     *
     * @return string
     */
    private static function parseParams(array $params): string
    {
        $aux = "";
        $currentIndex = 1;

        foreach ($params as $param) {
            $aux .= " | Param$currentIndex: " . $param;
            ++$currentIndex;
        }

        return $aux;
    }

    /**
     * @param string $flag
     * @param string $request
     * @param array $params
     *
     * @return string
     */
    private static function buildContent(
        string $flag,
        string $request,
        array $params = []
    ): string
    {
        $base = self::getCurrentTime() . $flag;

        if ($flag === self::LOG_FLAG_INFO) {
            return $base . ' - "Procesamos ' . $request . self::parseParams($params) . '"';
        }

        return $base . ' - "Hubo un error al intentar procesar ' . $request . self::parseParams($params) . '"';
    }

    /**
     * @param string $request
     *
     * @param array $params
     */
    public static function info(string $request, array $params = []): void
    {
        self::addLog(self::buildContent(self::LOG_FLAG_INFO, $request, $params));
    }

    /**
     * @param string $request
     */
    public static function error(string $request): void
    {
        self::addLog(self::buildContent(self::LOG_FLAG_ERROR, $request));
    }
}