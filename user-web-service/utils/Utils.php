<?php

/**
 * @param soap_server $server
 * @param string $namespace
 * @param string $functionName
 * @param array $inputs
 * @param array $outputs
 */
function registerService(
    soap_server $server,
    string $namespace,
    string $functionName,
    array $inputs,
    array $outputs
): void
{
    $server->register($functionName, $inputs, $outputs, $namespace);
}