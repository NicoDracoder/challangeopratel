<?php

require_once __DIR__ . '/../database/DAO.php';
require_once __DIR__ . '/../utils/Logger.php';

const REQUEST_GET_USER = "request getUser";
const REQUEST_ADD_USER = "request addUser";
const REQUEST_ACTIVATE_USER = "request activateUser";
const REQUEST_DEACTIVATE_USER = "request deactivateUser";

/**
 * @param string $username
 * @param string $email
 * @param string $password
 *
 * @return int
 */
function addUser(string $username, string $email, string $password): int
{
    Logger::info(REQUEST_ADD_USER, [$username, $email, $password]);
    $dao = new DAO();
    $user = $dao->getUserDAO($username);

    if ($user) {
        Logger::error(REQUEST_ADD_USER);

        return 400;
    }

    $dao->addUserDAO($username, $email, $password);

    return 200;
}

/**
 * @param string $username
 *
 * @return array|null
 */
function getUser(string $username): ?array
{
    Logger::info(REQUEST_GET_USER, [$username]);
    $dao = new DAO();

    return $dao->getUserDAO($username);
}

/**
 * @param string $username
 *
 * @return int
 */
function deactivateUser(string $username): int
{
    Logger::info(REQUEST_DEACTIVATE_USER, [$username]);
    $dao = new DAO();
    $user = $dao->getUserDAO($username);

    if (!$user) {
        Logger::error(REQUEST_DEACTIVATE_USER);

        return 404;
    }

    $isActive = false;
    $dao->changeActiveUserDAO($username, $isActive);

    return 200;
}

/**
 * @param string $username
 *
 * @return int
 */
function activateUser(string $username): int
{
    Logger::info(REQUEST_ACTIVATE_USER, [$username]);
    $dao = new DAO();
    $user = $dao->getUserDAO($username);

    if (!$user) {
        Logger::error(REQUEST_ACTIVATE_USER);

        return 404;
    }

    $isActive = true;
    $dao-> changeActiveUserDAO($username, $isActive);

    return 200;
}

