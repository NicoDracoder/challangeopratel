import { Schema, Document, model } from "mongoose";
import { IUser } from "../interfaces/IUser";

const User = new Schema(
  {
    username: { type: String },
    email: {
      type: String,
      required: true,
      unique: true,
      match: /\S+@\S+\.\S+/,
    },

    password: { type: String },
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

export default model<IUser & Document>("User", User);
