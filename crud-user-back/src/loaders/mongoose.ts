import mongoose from "mongoose";
import config from "../config";

const options = {
  useUnifiedTopology: true,
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
};

export default async () => {
  const connection = await mongoose.connect(
    `mongodb://${config.database.host}:${config.database.port}/${config.database.name}`,
    options
  );
  connection.connection.db
    ? console.log("connection successfully established")
    : console.log("error trying to connect");
};
