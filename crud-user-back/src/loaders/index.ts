import expressLoader from "./express";
import connectMongoose from "./mongoose";

export default async ({ expressApp }: { expressApp: any }) => {

  await expressLoader({ app: expressApp });
  await connectMongoose()
};
