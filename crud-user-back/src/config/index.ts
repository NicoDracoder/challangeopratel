import dotenv from "dotenv";

process.env.NODE_ENV = process.env.NODE_ENV || "development";

const envFound = dotenv.config();
if (!envFound) throw new Error("Couldn't find .env file ");

interface IConfig {
  port: string;
  api: {
    prefix: string;
  };
  database: {
    host: string;
    port: string;
    name: string;
  };
}

const conf: IConfig = {
  port: process.env.PORT,
  api: {
    prefix: "/api/v1",
  },
  database: {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    name: process.env.DB_NAME,
  },
};

export default conf;
