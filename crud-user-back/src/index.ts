import express from "express";
import config from "./config";

async function startServer() {
  const app = express();

  await require("./loaders").default({ expressApp: app });

  app.listen(config.port, () => {
    console.log(`User service is listening on ${config.port}`);
  });
}

startServer();
