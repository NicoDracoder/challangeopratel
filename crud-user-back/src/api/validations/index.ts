import { Joi } from "celebrate";

export const id_mongo_params = Joi.object({
  id: Joi.string().min(24).max(24).required(),
});

export const user_schema = Joi.object({
  email: Joi.string().required(),
  password: Joi.string().required(),
  username: Joi.string().required(),
})

export const user_schema_optional = Joi.object({
  email: Joi.string(),
  password: Joi.string(),
  username: Joi.string(),
})
