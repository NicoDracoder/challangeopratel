import { Router, Request, Response, NextFunction } from "express";
import * as UserService from "../../services/user.service";
import { celebrate, errors } from "celebrate";
import {
  id_mongo_params,
  user_schema,
} from "../validations";

const route = Router();

export default (app: Router) => {
  app.use("/users", route);
  app.use(errors());

  route.get("/", async (req: Request, res: Response, next: NextFunction) => {
    try {
      const users = await UserService.getAll();

      return res.status(200).json(users);
    } catch (e) {
      return next(e);
    }
  });
  route.get(
    "/:id",
    celebrate({
      params: id_mongo_params,
    }),
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const { id } = req.params;
        const user = await UserService.getOne(id);

        return res.status(200).json(user);
      } catch (e) {
        return next(e);
      }
    }
  );
  route.post(
    "/",
    celebrate({
      body: user_schema,
    }),
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const newUser = await UserService.add(req.body);

        return res.status(201).json(newUser);
      } catch (e) {
        return next(e);
      }
    }
  );
  route.put(
    "/:id",
    celebrate({
      params: id_mongo_params
    }),
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const { id } = req.params;
        const userUpdated = await UserService.update(id, req.body);

        return res.status(200).json(userUpdated);
      } catch (e) {
        return next(e);
      }
    }
  );
  route.delete(
    "/:id",
    celebrate({
      params: id_mongo_params,
    }),
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const { id } = req.params;
        const userRemoved = await UserService.remove(id);

        return res.status(200).json(userRemoved);
      } catch (e) {
        return next(e);
      }
    }
  );
};
