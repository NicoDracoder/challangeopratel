import moment from "moment";
import { IUser } from "../interfaces/IUser";
import UserModel from "../models/user.model";

export const add = async (userData: IUser) => {
  const user = await UserModel.create(userData);
  return user;
};
export const getAll = async () => {
  return await UserModel.find();
};
export const getOne = async (id: string) => {
  return await UserModel.findById(id);
};
export const update = async (id: string, newUserData: IUser) => {
  const user = await UserModel.findById(id);

  if (!user) {
    throw new Error("User not found");
  }

  user.email = newUserData.email || user.email;
  user.password = newUserData.password || user.password;
  user.username = newUserData.username || user.username;
  user.updatedAt = moment().toJSON();
  user.save();

  return user;
};
export const remove = async (id: string): Promise<IUser> => {
  const user = await UserModel.findById(id);

  if (!user) {
    throw new Error("User not found");
  }

  await UserModel.deleteOne({ _id: id });

  return user;
};
